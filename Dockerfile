FROM registry.gitlab.com/media-cloud-ai/workers/generic/rs_command_line_worker:0.3.2-ubuntu

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends curl=7.58.0-2ubuntu3.24 unzip=6.0-21ubuntu1.2 && \
    rm -rf /var/lib/apt/lists/* && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENV AMQP_QUEUE job_aws_cli

ENTRYPOINT [ "/entrypoint.sh" ]
